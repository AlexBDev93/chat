import Vue from 'vue'
import VueSocketio from 'vue-socket.io'
import _ from 'lodash'
import $ from 'jquery'

var socket = Vue.use(VueSocketio, 'http://localhost:1923');

var vm = new Vue({
  el : "#chat",
  data: {
    user: {
      name: null
    },
    message: '',
    room: null,
    rooms: [
      {
        name: 'general',
        messages: []
      },
      {
        name: 'game',
        messages: []
      }
    ],
  },
  sockets:{
    receiveMessage: function(data) {
      var room  = _.find(this.rooms, {name: data.room});
      if (room) {
        room.messages.push(this.formatMessage(data))
      }
    }
  },
  methods: {
    formatMessage : function (data) {
      return data.user.name+' : '+data.message;
    },
    sendMessage: function() {
      this.$socket.emit('emitMessage', {
        message: this.message,
        room: this.room,
        user: this.user
      });

      this.message = ''
    },
    changeRoom: function (room) {
      this.room = room
    },
    setPseudonyme: function (event) {
      var pseudonyme = $(event.currentTarget).val();

      if (pseudonyme.length) {
        this.user.name = pseudonyme;
        if (this.room === null) {
          this.room = 'general';
        }
      }
    }
  }
});

vm.use = socket;
